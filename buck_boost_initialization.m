close all
clear all
clc

Fsw = 1e+6;
C= 5.4e-6;      %Filter Capacitor Value
L = 0.18e-3;    %Filter Inductor Value
LESR = 0;
CESR = 0;
RFET = 0;
VDF =  0;
RDON = 0.00001;

Vs = 15;        %Supply dc voltage
Is = 2;         %Load current
vn = -9;        %nominal capacitor output voltage
in = 3.2;       %nominal inductor current

dn = -vn/(-vn+Vs);       %Nominal Duty

X0 = [1 1];
%      I  V        
Il0 = X0(1);
Vc0 = X0(2);

%% Control Parameters

alpha = 0.001;
%alpha = 0.001;

alphastr = ['Simulation of Buck-Boost Converter for Alpha: ',num2str(alpha)];
b = [(Vs - vn)/L; in/C];
Q = [L 0; 0 C];

%system state space model
A = [0          (1-dn)/L;
     -(1-dn)/C          0];
 B = [0     -1/L;
      1/C   0];
  b = [(Vs-vn)/L;
        in/C];
  Cs = [0 1];
  D = zeros(1,1);

%% Simulation Control
TSIM  = 0.015;

vscaler = 1.2;
vstep_per = (vscaler-1)*100;
vstep_time = 0.01;
iscaler = 0.9;
istep_per = (iscaler-1)*100;
istep_time = 0.005;
sine_dis = 0.25;

if sign(istep_per) == -1
is_str = ['\downarrow IL ',num2str(istep_per),'%'];
else
    is_str = ['\uparrow IL ',num2str(istep_per),'%'];
end

if sign(vstep_per) == -1
vs_str = ['\downarrow Vin ',num2str(vstep_per),'%'];
else
    vs_str = ['\uparrow Vin ',num2str(vstep_per),'%'];
end

sim('buck_boost_mdl',TSIM)

%Plotting
figure
hold on
subplot(4,1,1)
plot(iL)
xlabel('$$t$$','Interpreter','Latex','FontSize',14)
ylabel('$$A$$','Interpreter','Latex','FontSize',14)
title('Inductor Current')
t = text(istep_time,0.9*max(iL),is_str);

if sign(istep_per) == -1
t.Color = [0.6350 0.0780 0.1840];
else t.Color = [0.4660 0.6740 0.1880];
end
t.FontSize = 14;

subplot(4,1,2)
plot(vC)
xlabel('$$t$$','Interpreter','Latex','FontSize',14)
ylabel('$$V$$','Interpreter','Latex','FontSize',14)
title('Capacitor Voltage')
u = text(vstep_time,0.9*max(vC),vs_str);

if sign(vstep_per) == -1
u.Color = [0.6350 0.0780 0.1840];
else u.Color = [0.4660 0.6740 0.1880];
end
u.FontSize = 14;
subplot(4,1,3)
plot(Lyp)
xlabel('$$t$$','Interpreter','Latex','FontSize',14)
ylabel('$$V(x)$$','Interpreter','Latex','FontSize',14)
title('Lyapunov')

subplot(4,1,4)
plot(duty)
xlabel('$$t$$','Interpreter','Latex','FontSize',14)
ylabel('$$d_t(t)$$','Interpreter','Latex','FontSize',14)
title('Duty Cycle')
fig=gcf;
%fig.Units='normalized';
%fig.OuterPosition=[0 0 1 1];

%sgtitle(alphastr)

figure
hold on
iindex = length(v_traj)*istep_time/TSIM;
vindex = length(v_traj)*vstep_time/TSIM;
if vindex < iindex
plot(v_traj(1:vindex),i_traj(1:vindex),'k--');
plot(v_traj(vindex+1,iindex),i_traj(vindex+1,iindex),'r');
plot(v_traj(iindex+1,length(v_traj)),i_traj(iindex+1,length(v_traj)),'c');

else
plot(v_traj(1:iindex),i_traj(1:iindex),'k--');
plot(v_traj((iindex+1):(vindex)),i_traj((iindex+1):vindex),'r');
plot(v_traj((vindex+1):(length(v_traj))),i_traj((vindex+1):(length(v_traj))),'c');
    
end
%plot(v_traj(1:length(v_traj)),i_traj(1:length(i_traj)));
xline(vn,'r--','LineWidth',1.5);
xlabel('$$v(t)$$','Interpreter','Latex','FontSize',14)
ylabel('$$i(t)$$','Interpreter','Latex','FontSize',14)
